import { useQuery } from '@tanstack/react-query';
import { getHomePokemones, getPokesById } from 'api/home/Pokemones';

const key = "home_pokemones";

export function useHomePokemones() {
    return useQuery([key], getHomePokemones);
}

export function useGetPokeById(id) {
    return useQuery([`${id}`], () => getPokesById(id));
}