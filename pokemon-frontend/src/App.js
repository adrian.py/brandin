import Routes from './routes/index';
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import { ReactQueryDevtools } from '@tanstack/react-query-devtools';
import { useSelector } from 'react-redux';

function App() {
  const { dark_mode } = useSelector(state => state.Config);

  const queryClient = new QueryClient({
    defaultOptions: {
      queries: {
        refetchOnWindowFocus: false,
        refetchOnMount: false,
        refetchOnReconnect: false,
        retry: 1,
        staleTime: 5 * 36000,
      },
    },
  });
  
  return (
    <QueryClientProvider client={queryClient}>
      <div className={`App h-screen ${dark_mode}`}>
        <div className="dark:bg-gray-900">
          <Routes />
        </div>
      </div>
      <ReactQueryDevtools initialIsOpen={false} />
    </QueryClientProvider>
  );
}

export default App;
