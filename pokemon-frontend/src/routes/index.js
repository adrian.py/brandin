import { Routes, BrowserRouter, Route } from "react-router-dom";
import Home from "components/Home";
import Navbar from "components/layouts/Navbar";
import Login from "components/Login";
import Torneo from "components/torneo/Torneo";
import Entrenadores from "components/Entrenadores";

//Middlewares
import ProtectedGuest from "components/middlewares/ProtectedGuest";
import ProtectedLogin from "components/middlewares/ProtectedLogin";

const Router = () => {
  return (
    <BrowserRouter>
    <Navbar/>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/login" element={<ProtectedGuest><Login/></ProtectedGuest>} />
        <Route path="/torneo" element={<ProtectedGuest><Torneo/></ProtectedGuest>} />
        <Route path="/entrenadores" element={<ProtectedLogin><Entrenadores/></ProtectedLogin>} />
      </Routes>
    </BrowserRouter>
  );
};

export default Router;
