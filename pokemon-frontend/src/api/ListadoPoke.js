import axios from 'lib/pokeapi';

export const getListadoPerPage = async (pokeFrom) => {
    const { data } = await axios.get(`https://pokeapi.co/api/v2/pokemon?offset=${pokeFrom}&limit=44`);
    return data;
};