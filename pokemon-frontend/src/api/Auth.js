import axios from 'lib/axios';

export const makeLogin = async (datos) => {
    await axios.get('/sanctum/csrf-cookie');
    const { data } = await axios.post('api/auth/login', datos);
    return data;
};

export const logout = async () => {
    const { data } = await axios.post('api/auth/logout');
    return data;
}

export const getUser = async () => {
    const { data } = await axios.get('api/user');
    return data;
}
