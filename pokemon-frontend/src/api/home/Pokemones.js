import axios from '../../lib/pokeapi';

export const getHomePokemones = async () => {
    const { data } = await axios.get(`/pokemon?offset=1&limit=20`);
    return data;
};

export const getPokesById = async (id) => {
    const { data } = await axios.get(`/pokemon/${id}`);
    return data;
};