import axios from 'lib/axios';

export const setInscripcion = async (datos) => {
    const { data } = await axios.post('api/entrenadores', datos);
    return data;
};

export const getEntrenadores = async (pagination) => {
    const { data } = await axios.get(`api/listado_entrenadores?page=${pagination}`);
    return data;
};
