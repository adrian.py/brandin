import { SET_AUTH, RESET_AUTH } from '../types/Auth';
import { SET_USER, RESET_USER } from '../types/User';

//Auth
export const setAuth = (auth) => {
    return {
        type: SET_AUTH,
        payload: auth
    };
}

export const resetAuth = () => {
    return {
        type: RESET_AUTH
    };
}

//User
export const setUser = (user) => {
    return {
        type: SET_USER,
        payload: user
    };
}

export const resetUser = () => {
    return {
        type: RESET_USER
    };
}
