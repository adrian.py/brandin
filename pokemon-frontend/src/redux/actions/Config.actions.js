import { ENABLE_DARK_MODE, DISABLE_DARK_MODE } from '../types/Config';

//Dark Mode
export const enableDarkMode = () => {
    return {
        type: ENABLE_DARK_MODE
    };
}

export const disableDarkMode = () => {
    return {
        type: DISABLE_DARK_MODE
    };
}