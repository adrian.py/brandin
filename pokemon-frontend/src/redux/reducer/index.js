import { combineReducers } from "redux";

import Auth from './AuthReducer';
import Config from './ConfigReducer';

const rootReducer = combineReducers({
    Auth, Config
});

export default rootReducer;