import { SET_AUTH, RESET_AUTH } from '../types/Auth';
import { SET_USER, RESET_USER } from '../types/User';

const InitialState = {
    authentication: {},
    isLoggedIn: false,
    user: {}
};

export default function auth(state = InitialState, action) {
    switch (action.type) {
        case SET_AUTH:
            return {
                ...state,
                authentication: action.payload,
                isLoggedIn: true
            }
        case RESET_AUTH:
            return {
                ...state,
                authentication: {},
                isLoggedIn: false
            }
        case SET_USER:
            return {
                ...state,
                user: action.payload
            }
        case RESET_USER:
            return {
                ...state,
                user: {}
            }
        default:
            return state;
    }
}