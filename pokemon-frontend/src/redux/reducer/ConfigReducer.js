import { ENABLE_DARK_MODE, DISABLE_DARK_MODE } from '../types/Config';

const InitialState = {
    dark_mode: ''
};

export default function dark(state = InitialState, action) {
    switch (action.type) {
        case ENABLE_DARK_MODE:
            return {
                ...state,
                dark_mode: 'dark'
            }
        case DISABLE_DARK_MODE:
            return {
                ...state,
                dark_mode: ''
            }
        default:
            return state;
    }
}