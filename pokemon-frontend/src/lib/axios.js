import axios from 'axios';
const token = localStorage.getItem('state') !== null ? "Bearer " + JSON.parse(localStorage.getItem('state')).Auth.authentication.access_token : '';
const instance = axios.create({
    baseURL: process.env.REACT_APP_API_URL,
    headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'X-Requested-With': 'XMLHttpRequest',
        'Authorization': token
    },
    withCredentials: true
})

export default instance;