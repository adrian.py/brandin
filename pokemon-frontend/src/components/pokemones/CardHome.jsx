import React from "react";
import { useGetPokeById } from 'hooks/home/Pokemones';

const CardHome = ({ poke }) => {

  const info_poke = useGetPokeById(poke.name);

  return (
    <div className="CardHome">

      <div className={`w-full h-4/5 rounded-t-lg`}>
        {info_poke.data &&
          <img src={info_poke.data.sprites.other.dream_world.front_default} alt={poke.name} />
        }
      </div>
      <div className={`w-full h-1/5 bg-white shadow-md rounded-b-md py-2 dark:bg-gray-700`}>
        <p className={`text-terciary text-bold text-xl px-4 dark:text-white`}>{poke.name}</p>
      </div>
    </div>
  );
};

export default CardHome;