import React, { useState, useEffect } from 'react';
import { getListadoPerPage } from 'api/ListadoPoke';
import Loading from 'components/utilities/Loading';
import { setInscripcion } from 'api/Entrenadores';
import { Navigate } from 'react-router-dom';

const Torneo = () => {

    const [entrenador, setEntrenador] = useState({});
    const [equipo, setEquipo] = useState([]);

    const [pagina, setPagina] = useState([]);

    const handleChangeEntrenador = (e) => {
        setEntrenador({ ...entrenador, [e.target.name]: e.target.value })
    };

    const [onPage, setOnPage] = useState(1);

    const [isLoading, setIsLoading] = useState(false);

    const [success, setSuccess] = useState(false);

    const [errors, setErrors] = useState([]);

    useEffect(() => {
        getListadoPerPage(onPage)
            .then((response) => setPagina(response))
            .catch((e) => console.log(e));
    }, [onPage]);

    const getIdFromUrl = (url) => {
        let array = url.split("/");
        return array[array.length - 2];
    }

    const selectPoke = (id) => {
        if (equipo.includes(parseInt(id))) {
            setEquipo(equipo.filter((poke) => poke != parseInt(id)));
        } else {
            if (equipo.length < 6) {
                setEquipo([...equipo, parseInt(id)]);
            }
        }
    }

    const isSelected = (id) => {
        return equipo.includes(parseInt(id));
    }

    const style = (id) => {
        if (isSelected(parseInt(id))) {
            return "bg-white rounded-lg border-2 border-black hover-translate-y-0 shadow-lg";
        } else if (equipo.length === 6) {
            return "bg-gray-200/60 rounded-lg";
        } else {
            return "bg-white rounded-lg shadow-lg  hover:-translate-y-3 hover:shadow-xl";
        }
    }

    const sendRegisto = () => {
        setIsLoading(true);
        setInscripcion({ email: entrenador.email, nombre: entrenador.nombres, apellidos: entrenador.apellidos, fecha_nacimiento: entrenador.fecha_nacimiento, integrantes: equipo })
            .then(() => setSuccess(true))
            .catch((error) => {
                setIsLoading(false);
                setErrors(error.response.data.errors)
            });
    }

    if (success) {
        return <Navigate to="/" />;
    }
    
    return (
        <div className="h-full w-full mt-5">
            <div className="flex justify-center">
                <p className="text-3xl font-bold dark:text-white">Registro al torneo</p>
            </div>
            <div className="h-full w-full flex justify-center mt-3">
                <div className="h-full w-3/4 bg-gray-100 rounded-md">
                    <div className="flex justify-center mt-3">
                        <p className="text-md text-gray-700/80 font-bold">Información del Entrenador</p>
                    </div>
                    <div className="w-full flex mt-3">
                        <div className="w-1/4 ml-2">
                            <input
                                className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline dark:bg-gray-200"
                                name="email"
                                type="email"
                                placeholder="emal@example.com"
                                onChange={(e) => handleChangeEntrenador(e)}
                                required />
                            {errors.email && (<p className="text-red-400">{errors.email[0]}</p>)}
                        </div>
                        <div className="w-1/4 ml-2">
                            <input
                                className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline dark:bg-gray-200"
                                name="nombres"
                                type="text"
                                placeholder="Nombre(s)"
                                onChange={(e) => handleChangeEntrenador(e)}
                                required />
                           {errors.nombre && (<p className="text-red-400">{errors.nombre[0]}</p>)}
                        </div>
                        <div className="w-1/4 ml-2">
                            <input
                                className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline dark:bg-gray-200"
                                name="apellidos"
                                type="text"
                                placeholder="Apellidos"
                                onChange={(e) => handleChangeEntrenador(e)}
                                required />
                            {errors.apellidos && (<p className="text-red-400">{errors.apellidos[0]}</p>)}
                        </div>
                        <div className="w-1/4 ml-2 mr-2">
                            <input className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline dark:bg-gray-200"
                                type="date"
                                name="fecha_nacimiento"
                                data-date="" data-date-format="YYYY MMMM DD"
                                onChange={(e) => handleChangeEntrenador(e)}
                            />
                            {errors.fecha_nacimiento && (<p className="text-red-400">{errors.fecha_nacimiento[0]}</p>)}
                        </div>
                    </div>

                    <div className="flex justify-center mt-5">
                        <p className="text-md text-gray-700/80 font-bold">Elije tus 6 pokemones</p>

                    </div>
                    <div className="flex justify-center mt-5">
                        {errors.integrantes && (<p className="text-red-400">{errors.integrantes[0]}</p>)}
                    </div>

                    <div className="w-full flex justify-center mt-10 ml-3 mr-3">
                        <div className="grid grid-cols-11 gap-4">
                            {pagina.results &&
                                pagina.results.map((poke) => (
                                    <button key={poke.name} onClick={() => selectPoke(getIdFromUrl(poke.url))} className={(!isSelected(getIdFromUrl(poke.url)) && equipo.length == 6) ? 'pointer-events-none' : ''}>
                                        <div className={style(getIdFromUrl(poke.url))}>
                                            <img className="w-full" src={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${getIdFromUrl(poke.url)}.png`} alt={poke.name} />
                                            <div className="mx-2 flex justify-center">
                                                <p className="font-bold text-sm">{poke.name}</p>
                                            </div>
                                        </div>
                                    </button>
                                ))
                            }
                        </div>
                    </div>
                    <div className="mt-10 flex justify-end mx-5">
                        <nav className="isolate inline-flex -space-x-px rounded-md shadow-sm" aria-label="Pagination">
                            <button disabled={onPage == 1 ? true : false} onClick={() => setOnPage(onPage - 48)} className={`relative inline-flex items-center rounded-l-md border border-gray-300 bg-white px-2 py-2 text-sm font-medium text-gray-500 hover:bg-gray-50 ${pagina.previous == null && 'focus:outline-none'}`}>
                                <svg className="h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                    <path fillRule="evenodd" d="M12.79 5.23a.75.75 0 01-.02 1.06L8.832 10l3.938 3.71a.75.75 0 11-1.04 1.08l-4.5-4.25a.75.75 0 010-1.08l4.5-4.25a.75.75 0 011.06.02z" clipRule="evenodd" />
                                </svg>
                                <span>Anterior</span>
                            </button>
                            <button disabled={onPage >= 1248 ? true : false} onClick={() => setOnPage(onPage + 48)} className={`relative inline-flex items-center rounded-r-md border border-gray-300 bg-white px-2 py-2 text-sm font-medium text-gray-500 hover:bg-gray-50 ${pagina.next == null && 'focus:outline-none'}`}>
                                <span>Siguiente</span>
                                <svg className="h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                    <path fillRule="evenodd" d="M7.21 14.77a.75.75 0 01.02-1.06L11.168 10 7.23 6.29a.75.75 0 111.04-1.08l4.5 4.25a.75.75 0 010 1.08l-4.5 4.25a.75.75 0 01-1.06-.02z" clipRule="evenodd" />
                                </svg>
                            </button>
                        </nav>
                    </div>
                    <div className="flex justify-center mb-4">
                        <button onClick={(() => sendRegisto())} disabled={isLoading} className="bg-blue-500 flex-row dark:bg-gray-100 hover:bg-blue-400 dark:hover:bg-gray-300 text-white dark:text-black text-xl font-bold py-2 px-4 border-b-4 border-blue-700 dark:border-gray-500 hover:border-blue-500 dark:hover:border-gray-800 rounded">
                            <span className="flex">
                                <Loading flag={isLoading} />
                                Registrarme al torneo
                            </span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Torneo;