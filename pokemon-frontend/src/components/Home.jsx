import React from 'react';
import CardHome from './pokemones/CardHome';
import { useHomePokemones } from 'hooks/home/Pokemones';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';

const Home = () => {
    const home_pokes = useHomePokemones();
    const { isLoggedIn } = useSelector(state => state.Auth);
    return (
        <div className="h-screen w-full">
            <div className="h-1/3 w-full flex justify-center items-center">
                <div className="mt-10">
                    <Link to={isLoggedIn ? '/entrenadores' : '/torneo'} className="bg-blue-500 dark:bg-gray-100 hover:bg-blue-400 dark:hover:bg-gray-300 text-white dark:text-black text-5xl font-bold py-2 px-4 border-b-4 border-blue-700 dark:border-gray-500 hover:border-blue-500 dark:hover:border-gray-700 rounded">
                        {isLoggedIn ?
                            'Ver entrenadores'
                            :
                            '¡Inscribirme al torneo!'
                        }
                    </Link>
                </div>
            </div>
            <div className="h-1/3 w-full">
                <p className="flex justify-center dark:text-white">Conoce algunos pokemones del Torneo</p>
                <div className="h-full mt-10 flex-row w-auto items-center overflow-x-auto overscroll-x-contain flex space-x-6 overflow-y-hidden">
                    {home_pokes.data &&
                        home_pokes.data.results.map((poke) => (
                            <CardHome poke={poke} key={poke.name} />
                        ))
                    }
                </div>
            </div>
        </div>
    );
}
export default Home;