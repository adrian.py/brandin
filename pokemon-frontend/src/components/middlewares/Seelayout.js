import React from 'react';
import { useSelector } from "react-redux";

const Seelayout = ({element}) => {
    const { isLoggedIn } = useSelector(state => state.auth);
	if(isLoggedIn){
		return (element);
	}
	return null;
}
export default Seelayout;
