import React from 'react';
import { Navigate } from 'react-router-dom';
import { useSelector } from "react-redux";

const ProtectedLogin = ({ children }) => {
    const { isLoggedIn } = useSelector(state => state.Auth);
    return isLoggedIn ? children : <Navigate to="/login" />;
}

export default ProtectedLogin;