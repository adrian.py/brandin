import React from 'react';
import { Navigate } from 'react-router-dom';
import { useSelector } from "react-redux";

const ProtectedGuest = ({ children }) => {
    const { isLoggedIn } = useSelector(state => state.Auth);
    return !isLoggedIn ? children : <Navigate to="/" />;
}

export default ProtectedGuest;