import React, { useState } from "react";
import { makeLogin } from 'api/Auth';
import { useDispatch, useSelector } from 'react-redux';
import { setAuth } from 'redux/actions/Auth.actions';
import Loading from 'components/utilities/Loading';
import { Navigate } from 'react-router-dom';

const Login = () => {
  const dispatch = useDispatch();
  const [error, setError] = useState({});
  const [isLoading, setIsLoading] = useState(false);
  const [form, setForm] = useState({ email: '', password: '' });
  const { isLoggedIn } = useSelector(state => state.Auth);

  const sendLogin = () => {
    setError({});
    setIsLoading(true);
    makeLogin(form)
      .then((response) => dispatch(setAuth(response)))
      .catch((e) => setError(e))
      .finally(() => {
        setIsLoading(false);
      });
  }

  if (isLoggedIn) {
    return <Navigate to="/" />;
  }
  return (
    <div className="w-full h-screen flex justify-center mt-10">
      <form className="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4 w-1/2 h-1/2 dark:bg-gray-700 dark:shadow-gray-600">
        <div className="mb-4">
          <label
            className="block text-gray-700 text-sm font-bold mb-2 dark:text-white">
            Email
          </label>
          <input
            className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline dark:bg-gray-200"
            id="username"
            type="email"
            placeholder="email@example.com"
            required
            onChange={(e) => setForm({ ...form, 'email': e.target.value })}/>
        </div>
        <div className="mb-6">
          <label
            className="block text-gray-700 text-sm font-bold mb-2 dark:text-white">
            Contraseña
          </label>
          <input
            className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline dark:bg-gray-200"
            id="password"
            type="password"
            placeholder="******************"
            required
            onChange={(e) => setForm({ ...form, 'password': e.target.value })}/>
        </div>
        <div className="mb-5">
          {error.request && error.request.status === 401 &&
            <p className="text-red-500 text-xs italic">El usuario o la contraseña son incorrectos</p>
          }
        </div>
        <div className="flex justify-center">
          <button type='button' onClick={() => sendLogin()} disabled={isLoading} className="bg-blue-500 flex-row dark:bg-gray-100 hover:bg-blue-400 dark:hover:bg-gray-300 text-white dark:text-black text-xl font-bold py-2 px-4 border-b-4 border-blue-700 dark:border-gray-500 hover:border-blue-500 dark:hover:border-gray-800 rounded">
            <span className="flex">
              <Loading flag={isLoading} />
              Iniciar sesión
            </span>
          </button>
        </div>
      </form>
    </div>
  );
};

export default Login;
