import React, { useEffect, useState } from 'react';
import { getEntrenadores } from 'api/Entrenadores';
import Pagination from 'hooks/Pagination';
import {getPokesById} from 'api/home/Pokemones';

const Entrenadores = () => {

    const [entrenadores, setEntrenadores] = useState([]);
    const [pagination, setPagination] = useState(1);

    useEffect(() => {
        getEntrenadores(pagination)
            .then((response) => setEntrenadores(response))
            .catch((error) => console.log(error.response.data.errors));
    }, [pagination]);

    return (
        <div>
            <div className="w-full h-full flex justify-center mt-10">
                <div className="flex flex-col">
                    <div className="overflow-x-auto sm:-mx-6 lg:-mx-8">
                        <div className="py-2 inline-block min-w-full sm:px-6 lg:px-8">
                            <div className="overflow-hidden">
                                <table className="min-w-full">
                                    <thead className="border-b">
                                        <tr>
                                            <th scope="col" className="text-sm font-medium text-gray-900 px-6 py-4 text-left">Nombre(s)</th>
                                            <th scope="col" className="text-sm font-medium text-gray-900 px-6 py-4 text-left">Apellidos</th>
                                            <th scope="col" className="text-sm font-medium text-gray-900 px-6 py-4 text-left">Email</th>
                                            <th scope="col" className="text-sm font-medium text-gray-900 px-6 py-4 text-left">Poke 1</th>
                                            <th scope="col" className="text-sm font-medium text-gray-900 px-6 py-4 text-left">Poke 2</th>
                                            <th scope="col" className="text-sm font-medium text-gray-900 px-6 py-4 text-left">Poke 3</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {entrenadores.data &&
                                            entrenadores.data.map((entrenador) => (
                                                <tr className="border-b" key={entrenador.email}>
                                                    <td className="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">{entrenador.nombre}</td>
                                                    <td className="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">{entrenador.apellidos}</td>
                                                    <td className="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">{entrenador.email}</td>
                                                    <td className="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">{entrenador.integrantes[0].pokemon_id}</td>
                                                    <td className="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">{entrenador.integrantes[1].pokemon_id}</td>
                                                    <td className="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">{entrenador.integrantes[2].pokemon_id}</td>
                                                </tr>
                                            ))
                                        }
                                    </tbody>
                                </table>
                                <Pagination meta={entrenadores.meta} pagination={pagination} setPagination={setPagination}/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



        </div>
    );
}

export default Entrenadores;