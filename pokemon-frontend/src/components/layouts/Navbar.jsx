import React from "react";
import { Link } from 'react-router-dom';
import { enableDarkMode, disableDarkMode } from '../../redux/actions/Config.actions';
import { useDispatch, useSelector } from 'react-redux';
import { logout } from 'api/Auth';
import { resetAuth } from 'redux/actions/Auth.actions';

const Navbar = () => {
    const dispatch = useDispatch();
    const { dark_mode } = useSelector(state => state.Config);
    const { isLoggedIn } = useSelector(state => state.Auth);

    const log_out = () => {
        logout().then(() => {
            dispatch(resetAuth());
        });
    }
    
    return (
        <nav className="w-full h-14 bg-gradient-to-r from-white to-yellow-400 p-4 justify-between z-10 flex dark:from-gray-800 dark:to-yellow-500">
            <div className="w-auto h-auto">
                <Link to={'/'} >
                    <div className="flex items-start">
                        <img className="h-8 w-8 mr-5" src="/images/pikachu.png" alt="Pikachu"></img>
                        <p className="text-xl text-primary font-black dark:text-white">Torneo Pokemón</p>
                    </div>
                </Link>
            </div>
            <div className="flex space-x-6">
                <button type="button" className="font-medium rounded-full text-sm p-1 text-center inline-flex items-center hover:bg-yellow-600">
                    {dark_mode === '' ?
                        <svg onClick={() => dispatch(enableDarkMode())} width="20" height="20" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path className="fill-current text-black" d="M15.293 11.293C13.8115 11.9631 12.161 12.1658 10.5614 11.8742C8.96175 11.5826 7.48895 10.8105 6.33919 9.66079C5.18944 8.51104 4.41734 7.03824 4.12574 5.4386C3.83415 3.83897 4.03691 2.18846 4.70701 0.70697C3.52758 1.23998 2.49505 2.05117 1.69802 3.07093C0.900987 4.09069 0.363244 5.28859 0.130924 6.56186C-0.101395 7.83512 -0.0213565 9.14574 0.364174 10.3813C0.749705 11.6168 1.42922 12.7404 2.34442 13.6556C3.25961 14.5708 4.38318 15.2503 5.61871 15.6358C6.85424 16.0213 8.16486 16.1014 9.43813 15.8691C10.7114 15.6367 11.9093 15.099 12.9291 14.302C13.9488 13.5049 14.76 12.4724 15.293 11.293Z" fill="#3F3F46" />
                        </svg>
                        :
                        <svg onClick={() => dispatch(disableDarkMode())} className="w-6 h-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor">
                            <path className="fill-current text-white" strokeLinecap="round" strokeLinejoin="round" d="M12 3v2.25m6.364.386l-1.591 1.591M21 12h-2.25m-.386 6.364l-1.591-1.591M12 18.75V21m-4.773-4.227l-1.591 1.591M5.25 12H3m4.227-4.773L5.636 5.636M15.75 12a3.75 3.75 0 11-7.5 0 3.75 3.75 0 017.5 0z" />
                        </svg>
                    }
                </button>
                {isLoggedIn ?
                    <button onClick={() => log_out()} className="font-medium rounded-full text-sm p-1 text-center inline-flex items-center hover:bg-yellow-600">
                        <svg className="w-6 h-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor">
                            <path className="dark:text-white" strokeLinecap="round" strokeLinejoin="round" d="M15.75 9V5.25A2.25 2.25 0 0013.5 3h-6a2.25 2.25 0 00-2.25 2.25v13.5A2.25 2.25 0 007.5 21h6a2.25 2.25 0 002.25-2.25V15M12 9l-3 3m0 0l3 3m-3-3h12.75" />
                        </svg>
                    </button>
                    :
                    <Link to={'/login'} className="font-medium rounded-full text-sm p-1 text-center inline-flex items-center hover:bg-yellow-600">
                        <svg width="20" height="20" viewBox="0 0 25 27" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path className="fill-current text-black dark:text-white" fillRule="evenodd" clipRule="evenodd" d="M2.78225 17.6464C4.4799 15.2799 7.48651 13.4999 12.4999 13.4999C17.5132 13.4999 20.5199 15.2799 22.2175 17.6464C23.8582 19.9336 24.1196 22.5552 24.1248 24.0058C24.13 25.4645 22.9071 26.4166 21.6533 26.4166H3.34642C2.09262 26.4166 0.869709 25.4645 0.874956 24.0058C0.880174 22.5552 1.1415 19.9336 2.78225 17.6464Z" fill="#F2F2F2" />
                            <path className="fill-current text-black dark:text-white" fillRule="evenodd" clipRule="evenodd" d="M12.5 10.9166C15.3535 10.9166 17.6667 8.60339 17.6667 5.74992C17.6667 2.89645 15.3535 0.583252 12.5 0.583252C9.64653 0.583252 7.33334 2.89645 7.33334 5.74992C7.33334 8.60339 9.64653 10.9166 12.5 10.9166Z" fill="#F2F2F2" />
                        </svg>
                    </Link>
                }

            </div>
        </nav>
    )
}
export default Navbar;