/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  darkMode: 'class',
  theme: {
    extend: {
      backgroundImage: {
        'pikachu': "url('/public/images/pikachu.png')",
        'pokebola': "url('/public/images/pokebola.png')"
      },
    },
  },
  plugins: [],
}