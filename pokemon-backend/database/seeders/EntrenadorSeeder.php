<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\{Entrenador, Integrante};

class EntrenadorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Entrenador::factory()->count(100)->has(Integrante::factory()->count(6))->create();
    }
}
