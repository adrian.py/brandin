<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Carbon\Carbon;

class EntrenadorFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nombre' => $this->faker->name(),
            'email' => $this->faker->unique()->safeEmail(),
            'apellidos' => $this->faker->name(),
            'fecha_nacimiento' => $this->faker->date('Y-m-d', Carbon::now())
        ];
    }

    
}
