<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Entrenador;

class IntegranteFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
         return [
            'entrenador_id' => $this->faker->numberBetween(1,Entrenador::count()-1),
            'pokemon_id' => $this->faker->numberBetween(1, 100)
        ];
    }
}