<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\{EntrenadorController, TorneoController, AuthController};

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});*/


//Route::apiResource('entrenadores', EntrenadorController::class)->only(['store', 'show']);
//Route::get('entrenadores/listado/{pagination?}', [EntrenadorController::class, 'listado']);

Route::post('entrenadores', [EntrenadorController::class, 'store']);
Route::get('entrenadores/{id}', [EntrenadorController::class, 'show']);
Route::get('listado_entrenadores/{pagination?}', [EntrenadorController::class, 'listado']);

Route::group(['prefix' => 'auth'], function () {
	//Route::post('register', [AuthController::class, 'register']);
	Route::post('login', [AuthController::class, 'login']);
	Route::post('infouser', [AuthController::class, 'infoUser'])->middleware('auth:sanctum');
	Route::post('logout', [AuthController::class, 'logout'])->middleware('auth:sanctum');
});