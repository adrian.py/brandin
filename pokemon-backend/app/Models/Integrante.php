<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Integrante extends Model
{
    use HasFactory;
    protected $table = 'integrantes';
    protected $fillable = [
    	'entrenador_id',
    	'pokemon_id'
    ];

    public function entrenador(){
    	return $this->belongsTo(Entrenador::class, 'entrenador_id');
    }
}