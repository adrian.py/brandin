<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Entrenador extends Model
{
    use HasFactory;

    protected $table = 'entrenadores';
    protected $fillable = [
    	'nombre',
    	'email',
    	'apellidos',
    	'fecha_nacimiento'
    ];
    
    public function integrantes(){
    	return $this->hasMany(Integrante::class);
    }
}
