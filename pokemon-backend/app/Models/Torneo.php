<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Torneo extends Model
{
	use HasFactory;

	protected $table = 'torneos';
	protected $fillable = [
		'nombre',
		'is_disponible'
	];
}