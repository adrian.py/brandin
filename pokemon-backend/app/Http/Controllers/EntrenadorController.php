<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\{Entrenador, Integrante};
use App\Http\Requests\EntrenadorRequest;
use App\Http\Resources\EntrenadorResource;


class EntrenadorController extends Controller
{
    public function __construct(){
     $this->middleware('has_torneos', ['only' => ['store']]);
 }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function listado($pagination = 10){
        $entrenadores = Entrenador::paginate($pagination);
        return EntrenadorResource::collection($entrenadores);
    }
	/**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EntrenadorRequest $request){
        $entrenador = new Entrenador;
        $entrenador->nombre = $request->nombre;
        $entrenador->email = $request->email;
        $entrenador->apellidos = $request->apellidos;
        $entrenador->fecha_nacimiento = $request->fecha_nacimiento;
        $entrenador->save();
        foreach ($request->integrantes as $integrante) {
            Integrante::create(['entrenador_id' => $entrenador->id, 'pokemon_id' => $integrante]);
        }
        return response()->json([
            'status' => true,
            'entrenador' => new EntrenadorResource($entrenador),
            'class' => 'success',
            'message' => "Entrenador creado correctamente"
        ], 201);
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MedicoExterno  $medicoExterno
     * @return \Illuminate\Http\Response
     */
    public function show($id){
        if(Entrenador::where('id', $id)->exists()){
            return response()->json([
                'entrenador' => new EntrenadorResource(Entrenador::find($id))
            ], 200);
        }else{
            return response()->json([
                'status' => false,
                'class' => "warning",
                'message' => "Entrenador no encontrado"
            ], 404);
        }
    }
}
