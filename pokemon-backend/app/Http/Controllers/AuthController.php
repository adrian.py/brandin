<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\LoginRequest;

class AuthController extends Controller
{
    /*
    public function register(Request $request){
    	$validateData = $request->validate([
    		'name' => 'required|string|max:255',
    		'email' => 'required|email|unique:users|max:100',
    		'password' => 'required|string|min:8'
    	]);
    	$user = User::create([
    		'name' => $request->name,
    		'email' => $request->email,
    		'password' => Hash::make($request->password)
    	]);
    	$token = $user->createToken('auth_token')->plainTextToken;
    	return response()->json([
    		'access_token' => $token,
    		'token_type' => 'Bearer'
    	], 201);
    }
    */
    
    public function login(LoginRequest $request){
    	$credentials = [
            'email' => $request->email,
            'password' => $request->password,
        ];
    	if(!Auth::attempt($credentials)){
    		return response()->json([
    			'message' => "Invalid login details"
    		], 401);
    	}
    	$user = User::where('email', $request->email)->firstOrFail();
    	$token = $user->createToken('auth_token')->plainTextToken;
    	return response()->json([
    		'access_token' => $token,
    		'token_type' => 'Bearer'
    	], 201);
    }
    
    public function infoUser(Request $request){
    	return $request->user();
    }

    public function logout(Request $request){
    	$request->user()->currentAccessToken()->delete();
    	return response()->json([], 204);
    }
}
