<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Models\Torneo;

class TorneoIsDisponible
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if(Torneo::count() == 0){
            return response()->json([
                'status' => false,
                'class' => 'danger',
                'message' => "No hay torneos registrados"
            ], 404);
        }else{
            $lastTorneo = Torneo::latest()->first();
            if(!$lastTorneo->is_disponible){
                return response()->json([
                    'status' => false,
                    'class' => 'danger',
                    'message' => "Torneo cerrado"
                ], 404);
            }
        }
        return $next($request);
    }
}
