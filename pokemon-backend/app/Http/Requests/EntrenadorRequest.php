<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EntrenadorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(){
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(){
        return [
            'nombre' => 'required',
            'email' => 'required|email|unique:entrenadores|max:100',
            'apellidos' => 'required',
            'fecha_nacimiento' => 'required|date',
            'integrantes' => 'required|array',
            'integrantes.*' => 'integer',
        ];
    }

    public function messages(){
        return [
            'nombre.required' => 'El nombre es requerido',
            'email.required' => 'El email es requerido',
            'email.email' => 'El email debe terner un formato de email',
            'email.unique' => 'Email en uso, elija otro',
            'apellidos' => 'Apellidos son requeridos',
            'fecha_nacimiento.required' => 'Fecha de nacimiento es requerida',
            'fecha_nacimiento.date' => 'Fecha de nacimiento debe ser una fecha',
            //'fecha_nacimiento.date_format' => 'Formato de fecha incorrecto, formato dd/mm/YYYY',
            'integrantes.required' => 'Los integrantes del equipo son obligatorios',
            'integrantes.array' => 'El formato de los integrantes debe ser un array',
            'integrantes.*.integer' => 'Todos los integrantes deben ser integer'
        ];
    }
}